Pod::Spec.new do |s|
  s.name         = 'KC_Encryption'
  s.version      = '1.0'
  s.summary      = '[objective-c] NSString Encode and AES Encryption '
  s.author = {
    'Kennix' => 'kennixdev@gmail.com'
  }
  s.source = {
    :git => 'https://bitbucket.org/kennixpod/kc_encryption.git',
    :tag => '1.0'
  }
  s.source_files = 'KC_encryption/Src/*'
  s.platform     = :ios, "6.0"
  s.homepage = 'https://bitbucket.org/kennixpod/kc_encryption'
  s.license = 'MIT'

end


