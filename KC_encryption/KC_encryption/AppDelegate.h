//
//  AppDelegate.h
//  KC_encryption
//
//  Created by Kennix on 7/6/2015.
//  Copyright (c) 2015年 kennix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

